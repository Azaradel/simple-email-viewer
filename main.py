from datetime import datetime, timedelta
import sys
import json
import email.header
import email
import codecs

from PyQt5 import QtCore, QtWidgets, QtWebEngineWidgets
from imapclient import IMAPClient, exceptions


class User():

    def __init__(self, username, password, host):
        self.username = username
        self.password = password
        self.host = host


class LoginForm(QtWidgets.QWidget):

    authenticated = QtCore.pyqtSignal(object)
    canceled = QtCore.pyqtSignal()

    def __init__(self, *arg, **kwargs):
        super().__init__(*arg, **kwargs)

        self.username_input = QtWidgets.QLineEdit()
        self.password_input = QtWidgets.QLineEdit()
        self.host_input = QtWidgets.QLineEdit()

        try:
            with open('auth.json', 'r') as file:
                user = json.load(file)
                self.username_input.setText(user['username'])
                self.password_input.setText(user['password'])
                self.host_input.setText(user['host'])
        except Exception as e:
            print(e)
            pass

        self.password_input.setEchoMode(QtWidgets.QLineEdit.Password)

        self.cancel_button = QtWidgets.QPushButton('Cancel')
        self.submit_button = QtWidgets.QPushButton('Login')
        self.submit_button.setAutoDefault(True)
        layout = QtWidgets.QFormLayout()
        layout.addRow('Username', self.username_input)
        layout.addRow('Password', self.password_input)
        layout.addRow('Host', self.host_input)

        button_widget = QtWidgets.QWidget()
        button_widget.setLayout(QtWidgets.QHBoxLayout())
        button_widget.layout().addWidget(self.cancel_button)
        button_widget.layout().addWidget(self.submit_button)
        layout.addRow(button_widget)
        self.setLayout(layout)

        self.cancel_button.clicked.connect(self.on_cancel)
        self.submit_button.clicked.connect(self.on_submit)
        self.authenticated.connect(self.saveAuthenticationData)

    def on_submit(self):

        username = self.username_input.text()
        password = self.password_input.text()
        host = self.host_input.text()

        with IMAPClient(host=host) as connection:
            try:
                connection.login(username, password)
                QtWidgets.QMessageBox.information(
                    self, 'Success', 'You are logged in.')
                self.authenticated.emit(User(username, password, host))
                self.close()
            except exceptions.LoginError as e:
                QtWidgets.QMessageBox.critical(
                    self, 'Failed', 'You are not logged in.')
                print(e)

    def on_cancel(self):
        self.canceled.emit()

    @QtCore.pyqtSlot(object)
    def saveAuthenticationData(self, user):
        with open('auth.json', 'w') as file:
            json.dump(user, file, default=lambda o: o.__dict__,
                      sort_keys=True, indent=4)


class EmailWidget(QtWidgets.QWidget):

    emailLoaded = QtCore.pyqtSignal(object)

    def __init__(self, *arg, **kwargs):
        super().__init__(*arg, **kwargs)

        layout = QtWidgets.QGridLayout()

        layout.addWidget(QtWidgets.QLabel('Subject:'), 0, 0)
        self.subject = QtWidgets.QLineEdit('')
        self.subject.setReadOnly(True)
        layout.addWidget(self.subject, 0, 1)

        layout.addWidget(QtWidgets.QLabel('From:'), 1, 0)
        self.sender = QtWidgets.QLineEdit('')
        self.sender.setReadOnly(True)
        layout.addWidget(self.sender, 1, 1)

        layout.addWidget(QtWidgets.QLabel('Received:'), 2, 0)
        self.received = QtWidgets.QLineEdit('')
        self.received.setReadOnly(True)
        layout.addWidget(self.received, 2, 1)

        layout.addWidget(QtWidgets.QLabel('To:'), 3, 0)
        self.recipient = QtWidgets.QLineEdit('')
        self.recipient.setReadOnly(True)
        layout.addWidget(self.recipient, 3, 1)

        self.message = QtWebEngineWidgets.QWebEngineView()
        settings = self.message.settings()
        settings.setAttribute(settings.LocalContentCanAccessRemoteUrls, True)
        self.message.settings = settings
        layout.addWidget(self.message, 4, 0, 1, -1)

        self.previous_button = QtWidgets.QPushButton('Previous')
        layout.addWidget(self.previous_button, 5, 0)

        self.next_button = QtWidgets.QPushButton('Next')
        layout.addWidget(self.next_button, 5, 1)

        self.setLayout(layout)

        self.emailLoaded.connect(self.on_email_loaded)

    @QtCore.pyqtSlot(object)
    def on_email_loaded(self, currentEmail):
        body = ''
        metadata = email.message_from_bytes(currentEmail[b'RFC822'])
        msg = codecs.decode(currentEmail[b'BODY[TEXT]'], 'quopri')
        try:
            msg = msg.decode('utf-8')
        except:
            pass

        try:
            body = email.message_from_string(msg).get_payload()
        except:
            pass

        self.subject.setText(parse_header(metadata.get('Subject')))
        self.sender.setText(parse_header(metadata.get('From')))
        self.received.setText(metadata.get('Date'))
        self.recipient.setText(parse_header(metadata.get('To')))
        self.message.setHtml(body)
        self.message.show()


class MainWindow(QtWidgets.QMainWindow):

    clicked = QtCore.pyqtSignal()
    loaded = QtCore.pyqtSignal()
    emailIds = []
    current_position = 0

    def __init__(self):
        super().__init__()

        qtRectangle = self.frameGeometry()
        centerPoint = QtWidgets.QDesktopWidget().availableGeometry().center()
        qtRectangle.moveCenter(centerPoint)
        self.move(qtRectangle.topLeft())

        self.loginForm = LoginForm(windowTitle='Log into IMAP server')

        self.loginForm.authenticated.connect(self.login)
        self.loginForm.canceled.connect(self.cancel)

        self.emailWidget = EmailWidget()

        self.loaded.connect(self.on_load)

        self.setCentralWidget(self.loginForm)

        self.emailWidget.previous_button.clicked.connect(self.on_previous)
        self.emailWidget.next_button.clicked.connect(self.on_next)

        self.show()

    @ QtCore.pyqtSlot(object)
    def login(self, user):
        self.user = user
        self.setCentralWidget(self.emailWidget)
        with IMAPClient(self.user.host) as connection:
            connection.login(self.user.username, self.user.password)
            connection.select_folder('INBOX')
            messages = connection.search(
                [u'SINCE', datetime.now() - timedelta(days=7)])
            self.fetched = connection.fetch(messages, ['RFC822', 'BODY[TEXT]'])
            self.total = self.fetched.__len__()

            for msgId in self.fetched:
                email = self.fetched.get(msgId)
                if email is None:
                    self.fetched.pop(msgId)
                    continue
                self.emailIds.append(msgId)

            self.loaded.emit()

    @ QtCore.pyqtSlot()
    def cancel(self):
        self.close()

    @ QtCore.pyqtSlot()
    def on_load(self):
        self.setFixedSize(800, 600)
        self.current_position = self.emailIds.__len__() - 1
        self.currentEmail = self.fetched.get(
            self.emailIds[self.current_position])
        self.emailWidget.emailLoaded.emit(self.currentEmail)

    @QtCore.pyqtSlot()
    def on_next(self):
        self.current_position = self.current_position + 1
        if self.current_position == self.emailIds.__len__():
            QtWidgets.QMessageBox.information(
                self, 'Load next failed', 'No more emails to read')
            return

        self.currentEmail = self.fetched.get(
            self.emailIds[self.current_position])
        self.emailWidget.emailLoaded.emit(self.currentEmail)

    @QtCore.pyqtSlot()
    def on_previous(self):
        self.current_position = self.current_position - 1
        if self.current_position == self.emailIds.__len__():
            QtWidgets.QMessageBox.information(
                self, 'Load previous failed', 'No more emails to read')
            return

        self.currentEmail = self.fetched.get(
            self.emailIds[self.current_position])
        self.emailWidget.emailLoaded.emit(self.currentEmail)


def parse_header(header):
    parsed = ''
    header = email.header.decode_header(header)
    for h in header:
        if h[1] is not None:
            parsed = parsed + h[0].decode(h[1])
        elif type(h[0]) is bytes:
            parsed = parsed + h[0].decode('utf-8')
        else:
            parsed = parsed + h[0]

    return parsed

if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)

    main = MainWindow()

    sys.exit(app.exec_())
